import { IReporter } from "./interfaces/reporter.interface";
import { ITimeResultProvider } from "./interfaces/time-result-provider.interface";
import { Metadata } from "./types";
import { Measure } from "./measure";

const { green, red, gray, cyan, white } = require("chalk");

interface Stats {
  min: number;
  max: number;
  sum: number;
  count: number;
  length: number[];
  provider?: string;
  method?: string;
}

export default class StandardReporter implements IReporter {
  private stream: NodeJS.WritableStream;

  constructor() {
    this.stream = process.stdout;
  }

  onPerfStarted(name: string) {
    this.stream.write(white(`\nPerf: ${name}\n`));
  }

  private calculateStats(container: ITimeResultProvider): Stats {
    // Split results into runs to have total values
    const runContainers = container
      .getUniqueValuesOfMeta("run")
      .map((run) => container.filter("run", run));

    const empty: Stats = {
      min: Number.MAX_VALUE,
      max: 0,
      sum: 0,
      count: 0,
      length: [],
      provider: undefined,
    };

    let provider: string | undefined = undefined;
    let method: string | undefined = undefined;
    const calculated = runContainers.reduce(
      ({ sum, min, max, count, length }, container) => {
        // FIXME: Move it to better place
        if (!provider) {
          const v = container.iter().next().value as Measure;
          provider = v?.meta.provider;
          method = v?.meta.method;
        }

        const value = container.sum();
        const lengthInRun = container.count();
        return {
          sum: sum + value,
          min: min > value ? value : min,
          max: max < value ? value : max,
          count: count + 1,
          length: [...length, lengthInRun],
        };
      },
      empty
    );
    return {
      // TO milliseconds
      sum: Number.parseFloat((calculated.sum / 1000000).toFixed(2)),
      min: Number.parseFloat((calculated.min / 1000000).toFixed(2)),
      max: Number.parseFloat((calculated.max / 1000000).toFixed(2)),
      count: calculated.count,
      length: calculated.length,
      provider,
      method,
    };
  }

  onPerfFinished(container: ITimeResultProvider) {
    // Called
    container.getUniqueValuesOfMeta("called").forEach((call) => {
      const statsProvider = this.calculateStats(
        container.filter("called", call)
      );
      this.stream.write(
        red(`${statsProvider.provider} [${statsProvider.method || ""}]`) +
          cyan(`Called from ${call}: \n \t`) +
          gray(
            ` Mean: ${(statsProvider.sum / statsProvider.count).toFixed(
              2
            )}ms Min: ${statsProvider.min}ms Max: ${
              statsProvider.max
            }ms Times: (${Math.min(...statsProvider.length)}-${Math.max(
              ...statsProvider.length
            )})\n`
          )
      );
    });

    // Provider
    container.getUniqueValuesOfMeta("provider").forEach((provider) => {
      const statsProvider = this.calculateStats(
        container.filter("provider", provider)
      );
      this.stream.write(
        red(`Provider ${provider}\t:`) +
          gray(
            ` Mean: ${(statsProvider.sum / statsProvider.count).toFixed(
              2
            )}ms Min: ${statsProvider.min}ms Max: ${statsProvider.max}ms\n`
          )
      );
    });

    // For total
    const stats = this.calculateStats(container);
    this.stream.write(
      green(`\tTotal \t\t: `) +
        white(
          `Mean: ${(stats.sum / stats.count).toFixed(2)}ms Min: ${
            stats.min
          }ms Max: ${stats.max}ms\n`
        )
    );
  }
}
