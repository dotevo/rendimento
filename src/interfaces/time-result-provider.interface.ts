import { Metadata } from "../types/metadata.interface";
import { Measure } from "../measure";

export interface ITimeResultProvider {
  iter(): Generator<Measure, void, Measure>;
  clear(): void;

  mean(): number;
  max(): number;
  min(): number;
  sum(): number;
  count(): number;

  filter(key: Extract<keyof Metadata, string>, value: any): ITimeResultProvider;
  getUniqueValuesOfMeta(key: Extract<keyof Metadata, string>): any[];
}
