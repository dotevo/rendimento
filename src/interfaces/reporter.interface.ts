import { ITimeResultProvider } from "./time-result-provider.interface";

export interface IReporter {
  onPerfStarted?(name: string): void;
  onPerfFinished?(container: ITimeResultProvider): void;

  onRunStarted?(): void;
  onRunFinished?(container: ITimeResultProvider): void;
}
