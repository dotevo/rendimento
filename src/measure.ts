import { Metadata } from "./types/metadata.interface";

export class Measure {
  private _endTime: bigint | undefined;
  private _metadata: Metadata = {};

  private _time: number | undefined;

  constructor(public readonly startTime = process.hrtime.bigint()) {}

  public finish(
    metadata: Metadata = {},
    endTime: bigint = process.hrtime.bigint()
  ) {
    if (this._endTime) {
      return; // FIXME: Throw error
    }
    this._metadata = metadata;
    this._endTime = endTime;
    this._time = Number(this._endTime - this.startTime);
  }

  public get time(): number | undefined {
    return this._time;
  }

  public get endTime(): bigint | undefined {
    return this._endTime;
  }

  public get meta(): Metadata {
    return this._metadata;
  }
}
