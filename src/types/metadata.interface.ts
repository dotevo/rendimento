export interface Metadata {
  called?: string;
  method?: string;
  stack?: string;
  provider?: string;
  run?: number;
}
