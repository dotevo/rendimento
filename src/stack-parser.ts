export function extractCallLocation(stack: string, splitter: string): string {
  const cut = stack.slice(stack.lastIndexOf(splitter));
  const result = cut.match(/at [^(|\/]*([^)|\s]*)/);
  if (!result || !result[1]) {
    return "unknown";
  }
  return result[1].slice(result[1].lastIndexOf("/") + 1);
}
