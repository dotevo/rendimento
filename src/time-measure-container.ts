import { ITimeResultProvider } from "./interfaces/time-result-provider.interface";
import { Measure } from "./measure";
import { Metadata } from "./types/metadata.interface";

export class TimeMeasureContainer implements ITimeResultProvider {
  constructor(private measurements: Measure[] = []) {}

  static createPivot(
    containers: ITimeResultProvider[]
  ): TimeMeasureContainer[] {
    // Should be equal
    const length = containers.reduce((prevVal: number | undefined, newVal) => {
      const count = newVal.count();
      if ((prevVal ?? count) === count) {
        return count;
      } else {
        throw new Error();
      }
    }, undefined);
    if (length === undefined) {
      return [];
    }

    // FIXME: It can be wrong for async

    // Create array with measurements
    const iterators = containers.map((cont) => cont.iter());
    const result: TimeMeasureContainer[] = [];
    for (let p = 0; p < length; p++) {
      const items: Measure[] = [];
      for (const iter of iterators) {
        const value = iter.next().value;
        if (value) items.push(value);
      }
      const container = new TimeMeasureContainer(items);
      result.push(container);
    }
    return result;
  }

  public *iter(): Generator<Measure, void, Measure> {
    for (const meas of this.measurements) {
      yield meas;
    }
  }

  /**
   * Adds a new measure to container
   *
   * @param measure Measure given measure
   */
  public addTimeMeasure(measure: Measure) {
    this.measurements.push(measure);
  }

  /**
   * Clear data
   */
  public clear() {
    this.measurements = [];
  }

  /**
   * Returns a mean value of all measurements
   *
   * @return mean
   */
  public mean(): number {
    return this.sum() / this.count();
  }

  /**
   * Returns a total value of all measurements
   *
   * @return sum
   */
  public sum(): number {
    return this.measurements.reduce((result, el) => result + (el.time ?? 0), 0);
  }

  /**
   * Returns a min value of all measurements
   *
   * @return mean
   */
  public min(): number {
    return this.measurements.reduce((value: number, el): number => {
      if (!el.time) {
        return value;
      }
      return value > el.time ? el.time : value;
    }, Number.MAX_VALUE);
  }

  /**
   * Returns a max value of all measurements
   *
   * @return mean
   */
  public max(): number {
    return this.measurements.reduce((value: number, el): number => {
      if (!el.time) {
        return value;
      }
      return value < el.time ? el.time : value;
    }, 0);
  }

  /**
   * Returns count of all measurements
   *
   * @return mean
   */
  public count(): number {
    return this.measurements.length;
  }

  /**
   * Filter measurements
   *
   * @return a new container with filtered measurements
   */
  public filter(
    key: Extract<keyof Metadata, string>,
    value: any
  ): ITimeResultProvider {
    return new TimeMeasureContainer(
      this.measurements.filter((val) => {
        return (val.meta as any)[key] === value;
      })
    );
  }

  public getUniqueValuesOfMeta(key: Extract<keyof Metadata, string>): any[] {
    return Array.from(new Set(this.measurements.map((meas) => meas.meta[key])));
  }
}
