import TypedEmitter from "typed-emitter";
import { EventEmitter } from "events";
import { Metadata } from "./types/metadata.interface";
import { Measure } from "./measure";

export interface TimeMeasurerEvents {
  record: (measure: Measure) => void;
}

export abstract class TimeMeasurer extends (EventEmitter as new () => TypedEmitter<
  TimeMeasurerEvents
>) {
  // Started measurements
  private measurements: { [id: string]: Measure } = {};

  /**
   * It is called once before measurements.
   */
  abstract setUp(): void;
  /**
   * It is called once after measurements
   */
  abstract tearDown(): void;

  /**
   * Starts time measurement
   *
   * @param id string containing the unique id to pair it with end time
   */
  protected start(id: string) {
    this.measurements[id] = new Measure();
  }

  /**
   * Stops time measurement
   *
   * @param id string containing the unique id to pair it with start time
   * @param metadata Metadata contains additional information about measurement
   */
  protected stop(id: string, metadata: Metadata) {
    // FIXME: Check if exists

    // Add stack trace to metadata
    metadata.stack = metadata.stack || new Error().stack;
    this.measurements[id].finish(metadata);
    this.emit("record", this.measurements[id]);
    // Remove from object
    delete this.measurements[id];
  }
}
