import { TimeMeasurer } from "../time-measurer";
import { v4 as uuid } from "uuid";
import { Metadata } from "../types";
import { extractCallLocation } from "../stack-parser";
import * as path from "path";

export const obj = {
  testedMethod: function () {
    return 1;
  },
};

export class ExampleTimeMeasurer extends TimeMeasurer {
  private wrapped: (() => number) | undefined;

  setUp() {
    // Wrap method
    this.wrapped = obj.testedMethod.bind(obj);
    obj.testedMethod = (): number => {
      const id = uuid();
      const { stack } = new Error();
      const location = path.parse(__filename).name;

      const called = stack && extractCallLocation(stack, location);
      // Prepare metadata
      const meta: Metadata = { provider: "Example", stack, called };
      let val = 0;

      // If is wrapped
      if (this.wrapped) {
        //---- Start
        this.start(id);

        val = this.wrapped() + 1;

        this.stop(id, meta);
        //---- Stop
      }

      return val;
    };
  }

  tearDown() {
    if (this.wrapped) {
      obj.testedMethod = this.wrapped;
    }
  }
}
