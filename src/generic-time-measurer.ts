import { TimeMeasurer } from "./time-measurer";

import { v4 as uuid } from "uuid";
import * as path from "path";
import { extractCallLocation } from "./stack-parser";
import { Metadata } from "./types";

export class GenericTimeMeasurer<T> extends TimeMeasurer {
  constructor(
    public readonly methods: Extract<keyof T, string>[],
    private readonly obj: new (...args: any[]) => T
  ) {
    super();
  }

  setUp() {
    for (const method of this.methods) {
      this.replaceMethod(method as Extract<keyof T, string>);
    }
  }

  tearDown() {}

  protected replaceMethod(name: Extract<keyof T, string>) {
    const self = this;
    this.obj.prototype[`${name}Old`] = this.obj.prototype[name];
    // Wrapper
    this.obj.prototype[name] = async function (): Promise<any> {
      const id = uuid();
      const { stack } = new Error();
      // Get location: const location = path.parse(__filename).name;

      const called = stack && extractCallLocation(stack, self.obj.name);
      // Prepare metadata
      const meta: Metadata = {
        provider: self.obj.name,
        method: name,
        stack,
        called,
      };

      self.start(id);
      const results = this[`${name}Old`]();
      self.stop(id, meta);
      return results;
    };
  }
}
