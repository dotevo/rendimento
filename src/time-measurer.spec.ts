import { TimeMeasurer } from "./time-measurer";
import { ExampleTimeMeasurer, obj } from "./sample/example-time-measurer";
import { Measure } from "./measure";

describe("TimeMeasurer", () => {
  let timeMeasurer: TimeMeasurer;
  beforeAll(() => {
    timeMeasurer = new ExampleTimeMeasurer();
  });

  it("can set up and tear down", () => {
    timeMeasurer.setUp();
    expect(obj.testedMethod()).toEqual(2);
    timeMeasurer.tearDown();
    expect(obj.testedMethod()).toEqual(1);
  });

  it("emits event", () => {
    // Create dummy listener
    const listener = {
      record: (_measure: Measure) => {},
    };

    const spy = jest.spyOn(listener, "record");

    // Setup and add listener
    timeMeasurer.setUp();
    timeMeasurer.addListener("record", listener.record);

    // Run tested method
    obj.testedMethod();

    timeMeasurer.tearDown();
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
