import { Logger } from "./logger";
import { ExampleTimeMeasurer, obj } from "./sample/example-time-measurer";

describe("Logger", () => {
  it("can single run a test", async () => {
    // Create logger
    const logger = new Logger();

    // Add example measurer
    logger.addTimeMeasurer(new ExampleTimeMeasurer());

    await logger.perf("Performance test of tested method", async () => {
      obj.testedMethod();
    });
    expect(logger.container.count()).toEqual(1);

    logger.close();
  });

  it("can multiple runs a test", async () => {
    // Create logger
    const logger = new Logger();

    // Add example measurer
    logger.addTimeMeasurer(new ExampleTimeMeasurer());

    await logger.perf(
      "Performance test of tested method",
      async () => {
        obj.testedMethod();
      },
      5
    );
    expect(logger.container.count()).toEqual(5);

    logger.close();
  });
});
