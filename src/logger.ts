import { TimeMeasurer } from "./time-measurer";
import { TimeMeasureContainer } from "./time-measure-container";
import { ITimeResultProvider } from "./interfaces/time-result-provider.interface";
import { Metadata } from "./types/metadata.interface";
import { IReporter } from "./interfaces/reporter.interface";

import StandardReporter from "./standard-reporter";
import { Measure } from "./measure";

export class Logger {
  private measurers: TimeMeasurer[] = [];
  private _container: TimeMeasureContainer = new TimeMeasureContainer();
  private run: number = 0;

  constructor(private readonly reporter: IReporter = new StandardReporter()) {}

  public get container(): TimeMeasureContainer {
    return this._container;
  }

  public addTimeMeasurer(measurer: TimeMeasurer) {
    measurer.setUp();
    this.measurers.push(measurer);
    measurer.on("record", (measure) => {
      // Connect to run
      measure.meta.run = this.run;

      // Add result
      this._container.addTimeMeasure(measure);
    });
  }

  public newTestRun() {
    this.run += 1;
  }

  public close() {
    // Clear all
    for (const measurer of this.measurers) {
      measurer.tearDown();
    }
    this.measurers = [];
  }

  public clean() {
    this._container.clear();
    this.run = 0;
  }

  async perf(name: string, x: () => Promise<void>, runs: number = 1) {
    if (this.reporter.onPerfStarted) this.reporter.onPerfStarted(name);
    // Call method
    for (let p = 0; p < runs; p++) {
      this.newTestRun();
      if (this.reporter.onRunStarted) this.reporter.onRunStarted();
      await x();

      // Send result of test run
      if (this.reporter.onRunFinished) {
        // FIXME: !
        //this.reporter.onRunFinished(this.testRuns[this.testRuns.length - 1]);
      }
    }

    // Send all results
    if (this.reporter.onPerfFinished) {
      this.reporter.onPerfFinished(this._container);
    }
  }
}
