import { Measure } from "./measure";
import { TimeMeasureContainer } from "./time-measure-container";

describe("TimeMeasureContainer", () => {
  const m1 = new Measure(BigInt(100));
  m1.finish({ provider: "A" }, BigInt(200));

  const m2 = new Measure(BigInt(100));
  m2.finish({}, BigInt(150));

  it("can calculate min", () => {
    // Prepare measurements
    const container = new TimeMeasureContainer([m1, m2]);
    expect(container.min()).toEqual(50);
  });

  it("can calculate max", () => {
    // Prepare measurements
    const container = new TimeMeasureContainer([m1, m2]);
    expect(container.max()).toEqual(100);
  });

  it("can calculate mean", () => {
    // Prepare measurements
    const container = new TimeMeasureContainer([m1, m2]);
    expect(container.mean()).toEqual(75);
  });

  it("can calculate count", () => {
    // Prepare measurements
    const container = new TimeMeasureContainer([m1, m2]);
    expect(container.count()).toEqual(2);
  });

  it("can calculate filter", () => {
    // Prepare measurements
    const container = new TimeMeasureContainer([m1, m2]);
    expect(container.filter("provider", "A").count()).toEqual(1);
  });

  it("can iterate", () => {
    // Prepare measurements
    const container = new TimeMeasureContainer([m1, m2]);
    const iter = container.iter();
    expect(iter.next().value).toEqual(m1);
    expect(iter.next().value).toEqual(m2);
    expect(iter.next().done).toEqual(true);
  });
});
