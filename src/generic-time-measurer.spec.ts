import { Measure } from "./measure";
import { TimeMeasureContainer } from "./time-measure-container";
import { GenericTimeMeasurer } from "./generic-time-measurer";

class ToWrap {
  public method() {
    return 1;
  }
}

describe("GenericTimeMeasurer", () => {
  it("can wrap", async () => {
    let i = 0;
    const listener = (measure: Measure) => {
      i = i + 1;
    };
    const generic = new GenericTimeMeasurer<ToWrap>(["method"], ToWrap);
    generic.setUp();
    generic.addListener("record", listener);

    await new ToWrap().method();
    expect(i).toEqual(1);
  });
});
