#  About



## Example

```
import { Logger, GenericTimeMeasurer } from 'rendimento';
import { SelectQueryBuilder } from 'typeorm';

describe('Example', () => {
  let logger: Logger;

  beforeAll(async () => {
    logger = new Logger();

    logger.addTimeMeasurer(
      new GenericTimeMeasurer<SelectQueryBuilder<any>>(
        ['getOne', 'getMany', 'getRawOne', 'getRawMany'],
        SelectQueryBuilder,
      ),
    );
  }, 30000);

  it('Ex. test', async () => {
    await logger.perf(
      'Measure XYZ',
      async () => {
        await projection.getXYZ();
      },
      // Repeats
      10,
    );
  });
});

```

Ex. Output:

```
Ran all test suites.
SelectQueryBuilder [getRawMany]Called from user.projection.ts:20:10: 
         Mean: 5.19ms Min: 3.18ms Max: 27.91ms Times: (2-2)
SelectQueryBuilder [getRawMany]Called from company.projection.ts:19:93: 
         Mean: 0.82ms Min: 0.67ms Max: 1.59ms Times: (1-1)
...
Provider SelectQueryBuilder     : Mean: 13.80ms Min: 10.33ms Max: 62.39ms
Provider AnotherProvider        : Mean: 13.97ms Min: 10.27ms Max: 40.88ms
        Total           : Mean: 26.77ms Min: 20.6ms Max: 103.27ms
```